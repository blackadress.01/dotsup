call plug#begin()    
    Plug 'neoclide/coc.nvim', {'branch': 'release'}    
    Plug 'ap/vim-css-color'    
    Plug 'sheerun/vim-polyglot'    
    Plug 'mattn/emmet-vim'    
    Plug 'sbdchd/neoformat'    
    Plug 'gregsexton/matchtag'    
    Plug 'tpope/vim-commentary'    
    Plug 'Yggdroot/indentLine'    
    Plug 'prettier/vim-prettier', {    
  \ 'do': 'npm install',    
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }    
call plug#end()    
    
" Spaces & Tabs     
set tabstop=4       " number of visual spaces per TAB    
set softtabstop=4   " number of spaces in tab when editing    
set shiftwidth=4    " number of spaces to use for autoindent    
set expandtab       " tabs are space    
set smartindent    
set number    
let g:indentLine_char = '|'    
autocmd CursorHold * silent call CocActionAsync('highlight')  