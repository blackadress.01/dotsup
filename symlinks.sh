#!/bin/bash

ln -s ~/Dots/i3/config ~/.config/i3/config
ln -s ~/Dots/i3status/config ~/.config/i3status/config
ln -s ~/Dots/nvim/init.vim ~/.config/nvim/init.vim
ln -s ~/Dots/ranger/* ~/.config/ranger/
ln -s ~/Dots/.bash_profile ~/.bash_profile
ln -s ~/Dots/.bashrc ~/.bashrc
ln -s ~/Dots/.Xresources ~/.Xresources
ln -s ~/Dots/.xinitrc ~/.xinitrc

# just watched this vid https://www.youtube.com/watch?v=x1j2XC4J5Xg
# assuming the 'Dots' directory is located here "$HOME/Dots/"
# the script should provide an easier way to handle config files without having to rerun the script more than once
